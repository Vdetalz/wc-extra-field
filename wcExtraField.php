<?php
/*
Plugin Name: wcExtraField
Description: plugin add extra field to checkout.
Version: 1.0
Author: Vitaliy
*/

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	/**
	 * Add field
	 */
	add_action( 'woocommerce_after_order_notes', 'my_extra_information_checkout_field' );
	function my_extra_information_checkout_field( $checkout ) {

		echo '<div id="extra_field"><h3>' . _e( 'Additional Information' ) . '</h3>';

		woocommerce_form_field( 'extra_field', array(
			'type'        => 'textarea',
			'class'       => array( 'my-field-class form-row-wide' ),
			'label'       => __( 'Fill this field' ),
			'placeholder' => __( 'Additional Information' ),
		), $checkout->get_value( 'my_field_name' ) );

		echo '</div>';

	}

	/**
	 * Update meta data checkout
	 */
	add_action( 'woocommerce_checkout_update_order_meta', 'my_extra_information_checkout_field_update_order_meta' );
	function my_extra_information_checkout_field_update_order_meta( $order_id ) {
		if ( ! empty( $_POST['extra_field'] ) ) {
			update_post_meta( $order_id, 'Extra Information Field', sanitize_text_field( $_POST['extra_field'] ) );
		}
	}

	/**
	 * Display in admin pannel
	 */
	add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_extra_information_checkout_field_display_admin_order_meta', 10, 1 );
	function my_extra_information_checkout_field_display_admin_order_meta( $order ) {
		echo '<p><strong>' . _e( 'Additional information' ) . ':</strong> ' . get_post_meta( $order->id, 'Extra Information Field', true ) . '</p>';
	}

	/**
	 * Add field to mail
	 **/
	add_filter( 'woocommerce_email_order_meta_keys', 'my_extra_information_checkout_field_order_meta_keys' );
	function my_extra_information_checkout_field_order_meta_keys( $keys ) {
		$keys[] = 'Additional information';

		return $keys;
	}
}
